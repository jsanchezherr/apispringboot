package com.grupo02.bumer.dto;

import com.grupo02.bumer.model.Empresa;
import lombok.Data;

import java.util.List;

@Data
public class EmpresaDTO {
    Empresa empresa;
    List<ClienteDTO> listaClientes;
}
