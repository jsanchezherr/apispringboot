package com.grupo02.bumer.dto;

import com.grupo02.bumer.model.Empresa;
import lombok.Data;

@Data
public class Empresa2DTO {
    Empresa empresa;
    ClienteDTO clienteDTO;
}
