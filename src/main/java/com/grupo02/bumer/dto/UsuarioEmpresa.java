package com.grupo02.bumer.dto;

import com.grupo02.bumer.model.Empresa;
import com.grupo02.bumer.model.Usuario;
import lombok.Data;

@Data
public class UsuarioEmpresa {
    private Empresa empresa;
    private int idUsuario;
}
