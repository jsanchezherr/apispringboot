package com.grupo02.bumer.dto;

import lombok.Data;

@Data
public class ErroresComunes {
    private String message;
    private String codigo;
}
