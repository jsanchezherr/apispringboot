package com.grupo02.bumer.dto;

import lombok.Data;

@Data
public class UsuarioDTO {
     private int id;
     private String correo;
}
