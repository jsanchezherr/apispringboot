package com.grupo02.bumer.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class ClienteDTO implements Serializable {


    private Integer id;

    private String nombres;

    private String apellidos;

    private String dni;

}
