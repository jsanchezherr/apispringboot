package com.grupo02.bumer.dto;

import com.grupo02.bumer.model.Persona;
import lombok.Data;

@Data
public class UsuarioPersona {
    Persona persona;
    int idUsuario;
}
