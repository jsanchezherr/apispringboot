package com.grupo02.bumer.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class Empresa {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private long id;
    @Column(nullable = false, length = 50)
    private String razonSocial;
    @Column(nullable = false, length = 30)
    private String ruc;
    @Column(nullable = false, length = 50)
    private String rubro;

    @OneToOne
    @JoinColumn(name = "USER_ID")
    private Usuario usuario;

}
