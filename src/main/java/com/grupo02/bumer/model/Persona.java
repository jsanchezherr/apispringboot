package com.grupo02.bumer.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class Persona {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false, length = 50)
    private String nombres;
    @Column(nullable = false, length = 50)
    private String apellidoPaterno;
    @Column(nullable = false, length = 50)
    private String apellidoMaterno;
    @Column(nullable = false, length = 10)
    private String dni;
    private Integer edad;

    @OneToOne
    @JoinColumn(name = "USER_ID")
    private Usuario usuario;

}
