package com.grupo02.bumer.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class Usuario {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;
    @Column(nullable = false, length = 50)
    private String correo;
    @Column(nullable = false, length = 50)
    private String password;
    private String urlFoto;
    private boolean activo;
}
