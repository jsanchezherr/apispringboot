package com.grupo02.bumer.model;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Data
public class Empleo {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;
    private String puesto;
    private String area;
    private Integer cantidadPersona;
    private Double salario;
    private LocalDate fechaPublicacion;
    private LocalDate fechaFin;
    private String departamento;

}
