package com.grupo02.bumer.controller;


import com.grupo02.bumer.dto.UsuarioDTO;
import com.grupo02.bumer.model.Usuario;
import com.grupo02.bumer.services.IEmpresaService;
import com.grupo02.bumer.services.IPersonaService;
import com.grupo02.bumer.services.IUsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("bumer/api/usuario")
public class UsuarioController {


    IPersonaService personaService;
    IEmpresaService empresaService;
    IUsuarioService usuarioService;

    @Autowired
    public UsuarioController(IPersonaService personaService, IEmpresaService empresaService, IUsuarioService usuarioService){
        this.personaService = personaService;
        this.empresaService = empresaService;
        this.usuarioService = usuarioService;
    }


    @PostMapping("POST/v1/registrar-usuario")
    public ResponseEntity<UsuarioDTO> registrarUsuario(@RequestBody Usuario usuario) throws Exception {
        Usuario usuarioReturn = usuarioService.saveUsuario(usuario);
        if (usuarioReturn != null){
            UsuarioDTO usuarioDTO = new UsuarioDTO();
            usuarioDTO.setId(usuarioReturn.getId());
            usuarioDTO.setCorreo(usuarioReturn.getCorreo());
            return  new ResponseEntity<>(usuarioDTO, HttpStatus.CREATED);
        }else{
            return  new ResponseEntity<>(new UsuarioDTO(),HttpStatus.BAD_REQUEST);
        }
    }
}
