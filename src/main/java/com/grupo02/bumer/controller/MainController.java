package com.grupo02.bumer.controller;

import com.grupo02.bumer.dto.UsuarioDTO;
import com.grupo02.bumer.dto.UsuarioEmpresa;
import com.grupo02.bumer.dto.UsuarioPersona;
import com.grupo02.bumer.model.Empresa;
import com.grupo02.bumer.model.Persona;
import com.grupo02.bumer.model.Usuario;
import com.grupo02.bumer.services.IEmpresaService;
import com.grupo02.bumer.services.IPersonaService;
import com.grupo02.bumer.services.IUsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.List;

@RestController
@RequestMapping("bumer/api")
public class MainController {

    IPersonaService personaService;
    IEmpresaService empresaService;
    IUsuarioService usuarioService;

    @Autowired
    public MainController(IPersonaService personaService, IEmpresaService empresaService, IUsuarioService usuarioService){
        this.personaService = personaService;
        this.empresaService = empresaService;
        this.usuarioService = usuarioService;
    }







}
