package com.grupo02.bumer.controller;

import com.grupo02.bumer.dto.UsuarioPersona;
import com.grupo02.bumer.model.Persona;
import com.grupo02.bumer.model.Usuario;
import com.grupo02.bumer.services.IEmpresaService;
import com.grupo02.bumer.services.IPersonaService;
import com.grupo02.bumer.services.IUsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("bumer/api/persona")
public class PersonaController {

    IPersonaService personaService;
    IEmpresaService empresaService;
    IUsuarioService usuarioService;

    @Autowired
    public PersonaController(IPersonaService personaService, IEmpresaService empresaService, IUsuarioService usuarioService){
        this.personaService = personaService;
        this.empresaService = empresaService;
        this.usuarioService = usuarioService;
    }



    @PostMapping("POST/v1/registrar-persona")
    public ResponseEntity<UsuarioPersona> registrarPersona(@RequestBody(required = false) UsuarioPersona personaDTO){

        try {
            Usuario usuario = usuarioService.findUsuarioById(personaDTO.getIdUsuario());
            personaDTO.getPersona().setUsuario(usuario);
            Persona persona = personaService.savePersona(personaDTO.getPersona());

            personaDTO.setPersona(persona);
            personaDTO.setIdUsuario(personaDTO.getIdUsuario());
            return  new ResponseEntity<>(personaDTO, HttpStatus.CREATED);
        } catch (Exception err){
            System.out.println("ERROR: " + err.getMessage());
            return  new ResponseEntity<>(personaDTO,HttpStatus.BAD_REQUEST);
        }
    }

}
