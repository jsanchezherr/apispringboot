package com.grupo02.bumer.controller;

import com.grupo02.bumer.dto.*;
import com.grupo02.bumer.httpService.IClienteServiceRest;
import com.grupo02.bumer.model.Empresa;
import com.grupo02.bumer.model.Persona;
import com.grupo02.bumer.model.Usuario;
import com.grupo02.bumer.services.IEmpresaService;
import com.grupo02.bumer.services.IPersonaService;
import com.grupo02.bumer.services.IUsuarioService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.swing.text.html.Option;
import javax.websocket.server.PathParam;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("bumer/api/empresa")
@Api(value = "Api empresa", description = "Api de las empresas", tags = {"Api empresa*"})
public class EmpresaController {

    IPersonaService personaService;
    IEmpresaService empresaService;
    IUsuarioService usuarioService;
    IClienteServiceRest clienteServiceRest;

    @Autowired
    public EmpresaController(IPersonaService personaService, IEmpresaService empresaService, IUsuarioService usuarioService, IClienteServiceRest clienteServiceRest){
        this.personaService = personaService;
        this.empresaService = empresaService;
        this.usuarioService = usuarioService;
        this.clienteServiceRest = clienteServiceRest;
    }




    @PostMapping("POST/v1/registrar-empresa")
    public ResponseEntity<UsuarioEmpresa> registroEmpresa(@RequestBody(required = false) UsuarioEmpresa empresaDTO){

        try {
            Usuario usuario = usuarioService.findUsuarioById(empresaDTO.getIdUsuario());
            empresaDTO.getEmpresa().setUsuario(usuario);
            Empresa empresa = empresaService.saveEmpresa(empresaDTO.getEmpresa());

            empresaDTO.setEmpresa(empresa);
            empresaDTO.setIdUsuario(empresaDTO.getIdUsuario());
            return  new ResponseEntity<>(empresaDTO, HttpStatus.CREATED);
        } catch (Exception err){
            System.out.println("ERROR: " + err.getMessage());
            return  new ResponseEntity<>(empresaDTO,HttpStatus.BAD_REQUEST);
        }

    }


    @GetMapping("GET/v1/obtener-empresa/{id}")
    //documentacion con SWAGGER
    @ApiOperation(value = "Obtener empresa por ID", notes = "esto una nota", tags = {"Api empresa"})
    @ApiResponses({
            @ApiResponse(code = 200, message = "Esto un 200 OK", response = Persona[].class),
            @ApiResponse(code = 401, message = "Unauthorized ejem", response = ErroresComunes.class),
            @ApiResponse(code = 403, message = "Forbidden ejem", response = ErroresComunes.class),
            @ApiResponse(code = 404, message = "Not found ejm", response = ErroresComunes.class),
            @ApiResponse(code = 500, message = "Server internal error ejem", response = ErroresComunes.class)
    })
    //end
    public ResponseEntity<Empresa> obtenerEmpresaPorId(@PathVariable("id") int id) throws Exception{
        return  new ResponseEntity<>(empresaService.findEmpresaById(id),HttpStatus.CREATED);
    }

    @GetMapping("GET/v1/obtener-todas-empresas")
    public ResponseEntity<List<Empresa>> obtenerTodasEmpresas() throws Exception{
        return  new ResponseEntity<>(empresaService.findAll(),HttpStatus.OK);
    }

    @GetMapping("GET/v1/obtener-empresas-rubro")
    public ResponseEntity<List<Empresa>> obtenerTodasEmpresas(@PathParam("rubro") String rubro) throws Exception{
        return  new ResponseEntity<>(empresaService.findEmpresasByRubro(rubro),HttpStatus.OK);
    }


    @GetMapping("GET/v1/obtener-empresa/{id}/cliente/{idcliente}")
    public ResponseEntity<Empresa2DTO> obtenerEmpresaPorId(@PathVariable("id") int id, @PathVariable("idcliente") int idCliente) throws Exception{

        try{
            Empresa2DTO empresaDTO = new Empresa2DTO();
            empresaDTO.setEmpresa(Optional.ofNullable(empresaService.findEmpresaById(id)).orElse(new Empresa()));
            empresaDTO.setClienteDTO(Optional.ofNullable( clienteServiceRest.obtenerCliente(idCliente)).orElse( new ClienteDTO()));

            return  new ResponseEntity<>(empresaDTO,HttpStatus.CREATED);
        }catch (Exception err){
            return  new ResponseEntity<>(new Empresa2DTO(),HttpStatus.BAD_REQUEST);
        }

    }





}
