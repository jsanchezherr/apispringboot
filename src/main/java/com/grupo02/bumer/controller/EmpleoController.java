package com.grupo02.bumer.controller;


import com.grupo02.bumer.services.IEmpresaService;
import com.grupo02.bumer.services.IPersonaService;
import com.grupo02.bumer.services.IUsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("bumer/api/empleo")
public class EmpleoController {

    IPersonaService personaService;
    IEmpresaService empresaService;
    IUsuarioService usuarioService;

    @Autowired
    public EmpleoController(IPersonaService personaService, IEmpresaService empresaService, IUsuarioService usuarioService){
        this.personaService = personaService;
        this.empresaService = empresaService;
        this.usuarioService = usuarioService;
    }

}
