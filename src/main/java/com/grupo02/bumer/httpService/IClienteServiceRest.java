package com.grupo02.bumer.httpService;

import com.grupo02.bumer.dto.ClienteDTO;

import java.util.List;

public interface IClienteServiceRest {

    List<ClienteDTO> listar();

    ClienteDTO obtenerCliente(Integer id);

}
