package com.grupo02.bumer.repositories;

import com.grupo02.bumer.model.Empresa;
import com.grupo02.bumer.model.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IUsuarioRepository extends JpaRepository<Usuario,Integer> {
}
