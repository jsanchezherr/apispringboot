package com.grupo02.bumer.repositories;

import com.grupo02.bumer.model.Empleo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IEmpleoRepository extends JpaRepository<Empleo,Integer> {
}
