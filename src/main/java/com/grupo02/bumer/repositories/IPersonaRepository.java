package com.grupo02.bumer.repositories;

import com.grupo02.bumer.model.Empresa;
import com.grupo02.bumer.model.Persona;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface IPersonaRepository extends JpaRepository<Persona,Integer> {

}
