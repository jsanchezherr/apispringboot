package com.grupo02.bumer.services;

import com.grupo02.bumer.model.Usuario;

public interface IUsuarioService {
    Usuario findByCorreoAndPassword(String correo, String password) throws Exception;
    Usuario saveUsuario(Usuario usuario) throws Exception;
    boolean existUsuarioByCorreo(String correo) throws Exception;
    Usuario findUsuarioById(int id) throws Exception;
}
