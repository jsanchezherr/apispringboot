package com.grupo02.bumer.services;

import com.grupo02.bumer.model.Empleo;

import java.util.concurrent.ExecutionException;

public interface IEmpleoService {
    public Empleo saveEmpleo(Empleo empleo) throws Exception;
    public Empleo findEmpleoById(int id) throws Exception;
    public Empleo findEmpleoByPuesto(String puesto) throws Exception;
}
