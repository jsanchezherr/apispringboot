package com.grupo02.bumer.services;

import com.grupo02.bumer.model.Persona;

import java.util.List;

public interface IPersonaService {
    Persona savePersona(Persona persona) throws Exception;
    Persona findPersonaById(Integer id) throws Exception;
    List<Persona> findAll() throws Exception;
}
