package com.grupo02.bumer.services.impl;

import com.grupo02.bumer.model.Usuario;
import com.grupo02.bumer.repositories.IUsuarioRepository;
import com.grupo02.bumer.services.IUsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
public class UsuarioService implements IUsuarioService {

    @Autowired
    IUsuarioRepository usuarioDAO;

    @Override
    public Usuario findByCorreoAndPassword(String correo, String password) throws Exception {
        return usuarioDAO.findAll().stream().filter( usuario -> usuario.getCorreo().equalsIgnoreCase(correo) && usuario.getPassword().equalsIgnoreCase(password)).collect(Collectors.toList()).get(0);
    }

    @Override
    public Usuario saveUsuario(Usuario usuario) throws Exception {
        return usuarioDAO.save(usuario);
    }

    @Override
    public boolean existUsuarioByCorreo(String correo) throws Exception {
        return usuarioDAO.findAll().stream().anyMatch(usuario -> usuario.getCorreo().equalsIgnoreCase(correo));
    }

    @Override
    public Usuario findUsuarioById(int id) throws Exception {
        return usuarioDAO.getById(id);
    }
}
