package com.grupo02.bumer.services.impl;

import com.grupo02.bumer.model.Empresa;
import com.grupo02.bumer.model.Usuario;
import com.grupo02.bumer.repositories.IEmpresaRepository;
import com.grupo02.bumer.services.IEmpresaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmpresaService implements IEmpresaService {

    @Autowired
    IEmpresaRepository empresaDAO;

    @Override
    public Empresa saveEmpresa(Empresa empresa) throws Exception {

        return empresaDAO.save(empresa);
    }

    @Override
    public Empresa findEmpresaById(Integer id) throws Exception {




        return  empresaDAO.findAll().stream().filter(empresa -> empresa.getId() == id).collect(Collectors.toList()).get(0);
    }

    @Override
    public List<Empresa> findEmpresasByRubro(String rubro) throws Exception {
        return empresaDAO.findAll().stream().filter( empresa -> empresa.getRubro().equalsIgnoreCase(rubro)).collect(Collectors.toList());
    }

    @Override
    public List<Empresa> findAll() throws Exception {
        return empresaDAO.findAll();
    }

    @Override
    public Empresa findEmpresaByUsuario(Usuario usuario) throws Exception {
        return empresaDAO.findEmpresaByUsuario(usuario);
    }
}
