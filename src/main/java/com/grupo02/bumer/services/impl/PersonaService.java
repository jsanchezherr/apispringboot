package com.grupo02.bumer.services.impl;

import com.grupo02.bumer.model.Persona;
import com.grupo02.bumer.repositories.IPersonaRepository;
import com.grupo02.bumer.services.IPersonaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonaService implements IPersonaService {

    @Autowired
    IPersonaRepository personaDAO;

    @Override
    public Persona savePersona(Persona persona) throws Exception {
        return null;
    }

    @Override
    public Persona findPersonaById(Integer id) throws Exception {
        return null;
    }

    @Override
    public List<Persona> findAll() throws Exception {
        return null;
    }
}
