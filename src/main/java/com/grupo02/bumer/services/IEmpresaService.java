package com.grupo02.bumer.services;

import com.grupo02.bumer.model.Empresa;
import com.grupo02.bumer.model.Usuario;

import java.util.List;

public interface IEmpresaService {
    Empresa saveEmpresa(Empresa empresa) throws Exception;
    Empresa findEmpresaById(Integer id) throws Exception;
    List<Empresa> findEmpresasByRubro(String rubro) throws Exception;
    List<Empresa> findAll() throws Exception;
    Empresa findEmpresaByUsuario(Usuario usuario) throws Exception;
}
